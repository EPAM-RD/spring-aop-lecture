package ua.tvv.epam.lectures.aop.model;

import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Company {
    final String name;
    Set<Employee> employees = new HashSet<>();

    public Company hire(@NonNull Employee employee) {
        employees.add(employee);
        return this;
    }
}
