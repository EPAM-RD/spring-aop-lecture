package ua.tvv.epam.lectures.aop.authorization;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.StringJoiner;

import lombok.extern.slf4j.Slf4j;
import ua.tvv.epam.lectures.aop.exception.AccessDeniedException;
import ua.tvv.epam.lectures.aop.model.Employee;
import ua.tvv.epam.lectures.aop.model.Role;

import static java.util.Arrays.asList;

@Slf4j
@Component
@Aspect
public class AuthorizationAspect {
//
//    execution(modifiers-pattern? ret-type-pattern declaring-type-pattern? name-pattern(param-pattern)
//          throws-pattern?)

    @Pointcut("execution(public * ua.tvv.epam.lectures.aop.*.*(..))")
    private void anyPublicOperation(){}

    @Pointcut("@annotation(ua.tvv.epam.lectures.aop.authorization.Authorize)")
    private void authorizedMethod() {}

    @Before("@annotation(authorize)")
    public void authorize(JoinPoint joinPoint, Authorize authorize) {
        Employee employee = getPersonOrThrow(joinPoint);
        List<Role> allowedRoles = asList(authorize.value());

        if (!allowedRoles.contains(employee.getRole())) throw new AccessDeniedException(rolesRequiredMessage(allowedRoles));
    }

    @AfterReturning(value = "authorizedMethod()", returning = "result")
    public void gotResult(JoinPoint joinPoint, Object result) {
        Employee employee = getPersonOrThrow(joinPoint);

        log.debug("{} got access to {} ", employee, joinPoint.getSignature().toShortString());
    }


    private String rolesRequiredMessage(List<Role> allowedRoles) {
        StringJoiner joiner = new StringJoiner(" or ", "", " required");
        for (Role cs: allowedRoles) joiner.add(cs.name());
        return joiner.toString();
    }

    private Employee getPersonOrThrow(JoinPoint joinPoint) {
        final Object[] methodArguments = joinPoint.getArgs();

        for (Object methodArgument : methodArguments)
            if (methodArgument instanceof Employee) return (Employee) methodArgument;

        final String methodName = joinPoint.getSignature().toShortString();
        throw new IllegalStateException(methodName + " must accept Employee person as a parameter in order to authorize access");
    }
}
