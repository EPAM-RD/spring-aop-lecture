package ua.tvv.epam.lectures.aop.service;

import org.springframework.stereotype.Service;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import ua.tvv.epam.lectures.aop.exception.AccessDeniedException;
import ua.tvv.epam.lectures.aop.model.Account;
import ua.tvv.epam.lectures.aop.model.Employee;
import ua.tvv.epam.lectures.aop.persistant.Database;

import static ua.tvv.epam.lectures.aop.model.Role.ACCOUNTANT;
import static ua.tvv.epam.lectures.aop.model.Role.DIRECTOR;

@Service
@Slf4j
public class AccountService {

    private final Database database;

    public AccountService(Database database) {
        this.database = database;
    }

    public List<Account> getAccounts(Employee employee) {
        if (employee.getRole() != DIRECTOR && employee.getRole() != ACCOUNTANT) {
            throw new AccessDeniedException("DIRECTOR or ACCOUNTANT required");
        }

        List<Account> companyAccounts = database.selectCompanyAccounts();

        log.debug("{} got access to {} accounts", employee, companyAccounts.size());
        return companyAccounts;
    }

    //TODO: refactor with Aspects
}
