package ua.tvv.epam.lectures.aop.model;

public enum Role {
    DIRECTOR, ACCOUNTANT, DEVELOPER
}
