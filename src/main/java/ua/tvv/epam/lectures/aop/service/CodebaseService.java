package ua.tvv.epam.lectures.aop.service;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import ua.tvv.epam.lectures.aop.authorization.Authorize;
import ua.tvv.epam.lectures.aop.model.CodeChange;
import ua.tvv.epam.lectures.aop.model.Employee;
import ua.tvv.epam.lectures.aop.model.Role;
import ua.tvv.epam.lectures.aop.persistant.Database;

@Service
public class CodebaseService {
    private final Database codeDatabase;

    public CodebaseService(Database codeDatabase) {
        this.codeDatabase = codeDatabase;
    }

    @Authorize(Role.DEVELOPER)
    public LocalDateTime push(CodeChange codeChange, Employee employee) {
        codeDatabase.save(codeChange);
        return LocalDateTime.now();
    }
}
