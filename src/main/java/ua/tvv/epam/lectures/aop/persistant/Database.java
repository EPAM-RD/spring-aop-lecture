package ua.tvv.epam.lectures.aop.persistant;

import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import ua.tvv.epam.lectures.aop.model.Account;

@Component
public class Database {

    public List<Account> selectCompanyAccounts() {
        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account(randomAccountNumber()));
        accounts.add(new Account(randomAccountNumber()));
        accounts.add(new Account(randomAccountNumber()));
        return accounts;
    }

    public <T> T save(T entity) {
        return entity;
    }

    private String randomAccountNumber() {
        return MessageFormat.format("{0,number,.################}", Math.random()).substring(1);
    }
}
