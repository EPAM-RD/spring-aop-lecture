package ua.tvv.epam.lectures.aop;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.time.LocalDateTime;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import ua.tvv.epam.lectures.aop.model.Account;
import ua.tvv.epam.lectures.aop.model.CodeChange;
import ua.tvv.epam.lectures.aop.model.Employee;
import ua.tvv.epam.lectures.aop.model.Role;
import ua.tvv.epam.lectures.aop.service.AccountService;
import ua.tvv.epam.lectures.aop.service.CodebaseService;

@Slf4j
@Configuration
@EnableAspectJAutoProxy
@ComponentScan({
        "ua.tvv.epam.lectures.aop.persistant",
        "ua.tvv.epam.lectures.aop.authorization",
        "ua.tvv.epam.lectures.aop.service"
})
public class AopApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AopApplication.class);

        AccountService accountService = context.getBean(AccountService.class);
        CodebaseService codebaseService = context.getBean(CodebaseService.class);


        Employee vasia = new Employee("Vasia", Role.DIRECTOR);

        List<Account> companyAccounts = accountService.getAccounts(vasia);

        log.debug("Company accounts {}", companyAccounts);

        Employee petia = new Employee("Petia", Role.DEVELOPER);
        LocalDateTime pushTime = codebaseService.push(new CodeChange(), petia);
        log.warn("New code pushed at {}", pushTime);
    }
}